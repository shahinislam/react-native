import React from 'react'
import { Button, StyleSheet, Text, View, Alert } from 'react-native'

const App = () => {

  const pressHandler = () => {
    Alert.alert('Warning', 'You pressed the button.')
  }

  return (
    <View style={styles.body}>
      <Text>Shahinur Islam</Text>
      <Button
        color="indigo"
        title="Press"
        onPress={pressHandler}
      />
    </View>
  )
}

export default App

const styles = StyleSheet.create({
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})
